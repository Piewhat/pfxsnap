# pfxsnap

BTRFS snapshots for your proton prefixes

## Use Case

games that don't have Steam cloud  
recover from game save file corruption  
restore game save state to when you opened the game  
prefix geting wiped some how

or just make backups...

## Usage

add `pfxsnap& %command%` to the launch options in the game properties  
to set the amount of snapshot to keep set SNAPCOUNT like this `SNAPCOUNT=8 pfxsnap& %command%` the default amount is 5  
to set the minimum time to wait in seconds inbetween snapshots set SNAPWAIT like this `SNAPWAIT=8 SNAPCOUNT=8 pfxsnap& %command%` the default amount is 120  
the snapshots are stored in the games compatdata dir

## Limitations

I can not guarantee the safety of your data!  
the prefix must be on a BTRFS file system

## Install

```
git clone https://gitlab.com/Piewhat/pfxsnap
cd pfxsnap
cargo install --path=.
```

## Inspiration

the inspiration for this project is from GloriousEggroll's blunder that deleted my 32 hour Dark Souls 3 save :)

use std::{env, error::Error, fs, path::Path, process::Command};

use chrono::{DateTime, NaiveDateTime, Utc};

fn main() {
    let path = env::var("STEAM_COMPAT_DATA_PATH").expect("STEAM_COMPAT_DATA_PATH not set");
    let ammount = env::var("SNAPCOUNT").unwrap_or("5".into());
    let wait = env::var("SNAPWAIT").unwrap_or("120".into());
    // Check if compatdata path exists
    let path = Path::new(&path);
    if !path.exists() {
        return;
    }
    // Check if the subvolume marker file exists
    if !path.join("PFX_IS_SUBVOLUME").exists() {
        if let Err(err) = setup_subvolume(path) {
            eprintln!("Error in subvolume setup: {}", err);
            return;
        }
    }

    match snapshot_cleanup(
        path,
        ammount.parse().unwrap_or(5),
        wait.parse().unwrap_or(120),
    ) {
        Ok(s) => {
            if s {
                if let Err(err) = snapshot(path) {
                    eprintln!("Error taking snapshot: {}", err);
                    return;
                }
            }
        }
        Err(err) => eprintln!("Error cleaning snapshots: {}", err),
    }
}

fn setup_subvolume(path: &Path) -> Result<(), Box<dyn Error>> {
    // Rename the current pfx
    if let Err(err) = fs::rename(path.join("pfx"), path.join("pfx.psbak")) {
        return Err(err.into());
    }
    // Create the subvolume
    if let Err(err) = Command::new("btrfs")
        .arg("subvolume")
        .arg("create")
        .arg(path.join("pfx").as_os_str().to_str().unwrap())
        .output()
    {
        return Err(err.into());
    }

    // Copy the renamed pfx contents to the subvolume
    if let Err(err) = Command::new("cp") // Why does the std lib not have a way to do this?
        .arg("-r")
        .arg(path.join("pfx.psbak").join("."))
        .arg(path.join("pfx"))
        .output()
    {
        return Err(err.into());
    }

    // Create the subvolume marker
    fs::File::create(path.join("PFX_IS_SUBVOLUME"))?;

    // Make the snapshots dir
    if let Err(err) = fs::create_dir(path.join("snapshots")) {
        return Err(err.into());
    }

    Ok(())
}

fn snapshot(path: &Path) -> Result<(), Box<dyn Error>> {
    // Snapshot the pfx
    Command::new("btrfs")
        .arg("subvolume")
        .arg("snapshot")
        .arg(path.join("pfx").as_os_str().to_str().unwrap())
        .arg(
            path.join("snapshots")
                .join(chrono::Utc::now().to_rfc2822().to_string())
                .as_os_str()
                .to_str()
                .unwrap(),
        )
        .output()
        .expect("failed to create snapshot");

    Ok(())
}

fn snapshot_cleanup(path: &Path, ammount: usize, wait: i64) -> Result<bool, Box<dyn Error>> {
    // Check if there are too many snapshots
    let paths = fs::read_dir(path.join("snapshots"))?;
    if paths.count() < ammount {
        return Ok(true);
    }

    let paths = fs::read_dir(path.join("snapshots"))?;
    let mut oldest = (None, Utc::now().naive_utc());
    let mut newest = NaiveDateTime::from_timestamp(0, 0);
    for path in paths {
        let path = path?;

        let created = DateTime::parse_from_rfc2822(path.file_name().to_str().unwrap())
            .unwrap()
            .naive_utc();

        if created > newest {
            newest = created;
        }

        if created < oldest.1 {
            oldest = (Some(path), created);
        }
    }

    // Don't clenup if the snapshot is recent
    if newest > Utc::now().naive_utc() - chrono::Duration::seconds(wait) {
        return Ok(false);
    }

    fs::remove_dir_all(oldest.0.unwrap().path())?;

    Ok(true)
}

// This could have been a shell script but oh well
